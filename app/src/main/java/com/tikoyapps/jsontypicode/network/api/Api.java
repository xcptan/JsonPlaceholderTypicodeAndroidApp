package com.tikoyapps.jsontypicode.network.api;

import com.tikoyapps.jsontypicode.network.models.response.Album;
import com.tikoyapps.jsontypicode.network.models.response.Photo;
import java.util.ArrayList;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by xcptan on 3/22/16.
 */
public interface Api {

    @GET("/albums")
    Observable<ArrayList<Album>> getAlbums();

    @GET("/photos")
    Observable<ArrayList<Photo>> getAlbumPhotos(@Query("albumId") String albumId);
}

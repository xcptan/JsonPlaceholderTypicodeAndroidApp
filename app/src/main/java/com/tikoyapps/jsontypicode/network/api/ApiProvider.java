package com.tikoyapps.jsontypicode.network.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tikoyapps.jsontypicode.BuildConfig;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by xcptan on 3/22/16.
 */
public class ApiProvider {

    private static Api sApi;

    private static Gson sGson;

    public static Gson getGson() {
        if (sGson == null) {
            synchronized (ApiProvider.class) {
                if (sGson == null) {
                    sGson = new GsonBuilder().disableHtmlEscaping()
                        .excludeFieldsWithoutExposeAnnotation()
                        .create();
                }
            }
        }
        return sGson;
    }

    public static Api getApi() {
        if (sApi == null) {
            synchronized (ApiProvider.class) {
                if (sApi == null) {
                    sApi = new RestAdapter.Builder().setLogLevel(
                        BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                        .setEndpoint("http://jsonplaceholder.typicode.com")
                        .setConverter(new GsonConverter(getGson()))
                        .build()
                        .create(Api.class);
                }
            }
        }
        return sApi;
    }
}

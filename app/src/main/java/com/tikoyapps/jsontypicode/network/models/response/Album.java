package com.tikoyapps.jsontypicode.network.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by xcptan on 3/22/16.
 */
public class Album {

    @Expose
    @SerializedName("id")
    private int albumId;

    @Expose
    @SerializedName("userId")
    private int userId;

    @Expose
    @SerializedName("title")
    private String title;

    public int getAlbumId() {
        return albumId;
    }

    public int getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }
}

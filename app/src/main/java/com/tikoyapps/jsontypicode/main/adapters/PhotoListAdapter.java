package com.tikoyapps.jsontypicode.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.tikoyapps.jsontypicode.R;
import com.tikoyapps.jsontypicode.network.models.response.Photo;
import java.util.List;

/**
 * Created by xcptan on 3/22/16.
 */
public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.PhotoListVH> {

    public interface OnPhotoItemClicked {
        void onPhotoItemClicked(Photo photo);
    }

    private OnPhotoItemClicked mOnPhotoItemClicked;

    private LayoutInflater mLayoutInflater;
    private List<Photo> mPhotoList;
    private Context mContext;

    public PhotoListAdapter(Context context, List<Photo> photoList,
        OnPhotoItemClicked onPhotoItemClicked) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mPhotoList = photoList;
        mOnPhotoItemClicked = onPhotoItemClicked;
    }

    @Override
    public PhotoListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoListVH(mLayoutInflater.inflate(R.layout.photo_list_item, parent, false));
    }

    public void updateList(List<Photo> photoList) {
        mPhotoList = photoList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(PhotoListVH holder, int position) {
        final Photo photo = mPhotoList.get(position);
        holder.photoTitle.setText(photo.getTitle());
        Picasso.with(mContext).load(photo.getThumbnailUrl()).into(holder.thumbnail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnPhotoItemClicked.onPhotoItemClicked(photo);
            }
        });
        holder.itemView.setTag(photo);
    }

    @Override
    public int getItemCount() {
        return mPhotoList.size();
    }

    public static class PhotoListVH extends RecyclerView.ViewHolder {
        TextView photoTitle;
        ImageView thumbnail;

        public PhotoListVH(View itemView) {
            super(itemView);
            photoTitle = (TextView) itemView.findViewById(R.id.photo_list_item_title);
            thumbnail = (ImageView) itemView.findViewById(R.id.photo_list_item_thumbnail);
        }
    }
}

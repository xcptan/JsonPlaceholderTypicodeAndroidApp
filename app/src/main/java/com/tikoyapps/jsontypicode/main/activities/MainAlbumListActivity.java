package com.tikoyapps.jsontypicode.main.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.tikoyapps.jsontypicode.R;
import com.tikoyapps.jsontypicode.main.adapters.AlbumListAdapter;
import com.tikoyapps.jsontypicode.network.api.ApiProvider;
import com.tikoyapps.jsontypicode.network.models.response.Album;
import com.tikoyapps.jsontypicode.network.models.response.Photo;
import java.util.ArrayList;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by xcptan on 3/22/16.
 */
public class MainAlbumListActivity extends AppCompatActivity {

    private RecyclerView mAlbumList;
    private ProgressBar mProgressBar;
    private AlbumListAdapter mAlbumListAdapter;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_album_list);
        mAlbumList = (RecyclerView) findViewById(R.id.album_list);
        mProgressBar= (ProgressBar) findViewById(R.id.activity_main_album_list_loading);

        mAlbumListAdapter = new AlbumListAdapter(this, new ArrayList<Album>(),
            new AlbumListAdapter.OnAlbumItemClicked() {
                @Override
                public void onAlbumItemClicked(Album album) {
                    Intent intent = new Intent(MainAlbumListActivity.this,PhotoListActivity.class);
                    intent.putExtra("albumId", String.valueOf(album.getAlbumId()));
                    startActivity(intent);
                }
            });
        mAlbumList.setAdapter(mAlbumListAdapter);

        RecyclerView.LayoutManager layoutManager =
            new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mAlbumList.setLayoutManager(layoutManager);

        ApiProvider.getApi()
            .getAlbums()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(new Action1<ArrayList<Album>>() {
                @Override
                public void call(ArrayList<Album> albums) {
                    mProgressBar.setVisibility(View.GONE);
                    mAlbumListAdapter.updateList(albums);
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    Toast.makeText(MainAlbumListActivity.this, "Error occurred", Toast.LENGTH_SHORT).show();
                }
            });
    }
}

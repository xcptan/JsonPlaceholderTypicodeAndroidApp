package com.tikoyapps.jsontypicode.main.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tikoyapps.jsontypicode.R;
import com.tikoyapps.jsontypicode.network.models.response.Album;
import java.util.List;

/**
 * Created by xcptan on 3/22/16.
 */
public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.AlbumListVH> {

    public interface OnAlbumItemClicked {
        void onAlbumItemClicked(Album album);
    }

    private OnAlbumItemClicked mOnAlbumItemClicked;

    private LayoutInflater mLayoutInflater;
    private List<Album> mAlbumList;

    public AlbumListAdapter(Context context, List<Album> albumList, OnAlbumItemClicked onAlbumItemClicked) {
        mLayoutInflater = LayoutInflater.from(context);
        mAlbumList = albumList;
        mOnAlbumItemClicked = onAlbumItemClicked;
    }

    @Override
    public AlbumListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlbumListVH(mLayoutInflater.inflate(R.layout.album_list_item,parent,false));
    }

    public void updateList(List<Album> albumList){
        mAlbumList = albumList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(AlbumListVH holder, int position) {
        final Album album = mAlbumList.get(position);
        holder.albumTitle.setText(album.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnAlbumItemClicked.onAlbumItemClicked(album);
            }
        });
        holder.itemView.setTag(album);
    }

    @Override
    public int getItemCount() {
        return mAlbumList.size();
    }

    public static class AlbumListVH extends RecyclerView.ViewHolder {
        TextView albumTitle;

        public AlbumListVH(View itemView) {
            super(itemView);
            albumTitle = (TextView) itemView.findViewById(R.id.album_list_item_title);
        }
    }
}

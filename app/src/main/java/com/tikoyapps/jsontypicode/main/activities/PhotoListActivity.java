package com.tikoyapps.jsontypicode.main.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.tikoyapps.jsontypicode.R;
import com.tikoyapps.jsontypicode.main.adapters.PhotoListAdapter;
import com.tikoyapps.jsontypicode.network.api.ApiProvider;
import com.tikoyapps.jsontypicode.network.models.response.Photo;
import java.util.ArrayList;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by xcptan on 3/22/16.
 */
public class PhotoListActivity extends AppCompatActivity {

    private RecyclerView mPhotoList;
    private PhotoListAdapter mPhotoListAdapter;
    private ProgressBar mProgressBar;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);
        mPhotoList = (RecyclerView) findViewById(R.id.photo_list);
        mProgressBar = (ProgressBar) findViewById(R.id.activity_photo_list_loading);

        mPhotoListAdapter = new PhotoListAdapter(this, new ArrayList<Photo>(),
            new PhotoListAdapter.OnPhotoItemClicked() {

                @Override
                public void onPhotoItemClicked(Photo photo) {
                    Intent intent = new Intent(PhotoListActivity.this, PhotoActivity.class);
                    intent.putExtra("url", photo.getUrl());
                    startActivity(intent);
                }
            });
        mPhotoList.setAdapter(mPhotoListAdapter);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        mPhotoList.setLayoutManager(layoutManager);

        ApiProvider.getApi()
            .getAlbumPhotos(getIntent().getStringExtra("albumId"))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(new Action1<ArrayList<Photo>>() {
                @Override
                public void call(ArrayList<Photo> photos) {
                    mProgressBar.setVisibility(View.GONE);
                    mPhotoListAdapter.updateList(photos);
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    Toast.makeText(PhotoListActivity.this, "Error occurred", Toast.LENGTH_SHORT)
                        .show();
                }
            });
    }
}

package com.tikoyapps.jsontypicode.main.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.tikoyapps.jsontypicode.R;

/**
 * Created by xcptan on 3/22/16.
 */
public class PhotoActivity extends AppCompatActivity {

    private ImageView photoImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        photoImageView = (ImageView)findViewById(R.id.activity_photo_imageview);
        Picasso.with(this).load(getIntent().getStringExtra("url")).into(photoImageView);
    }
}
